#!/bin/bash -e

if [ -L /usr/bin/delete_stack ]; then
  . /usr/bin/delete_stack
  . /usr/bin/get_stack_status
else
  . $CFN_DEPLOY_PATH/delete_stack.sh
  . $CFN_DEPLOY_PATH/get_stack_status.sh
fi

delete_change_set() {
  echo "Deleting the change set $CFN_CHANGE_SET_NAME from $CFN_STACK_NAME..."
  aws cloudformation delete-change-set --stack-name $CFN_STACK_NAME --change-set-name $CFN_CHANGE_SET_NAME
  echo "Change set deleted."
  if get_stack_status; then
    case $stack_status in
      "REVIEW_IN_PROGRESS")
        # delete REVIEW_IN_PROGRESS stacks
        echo "$CFN_STACK_NAME will be deleted since it is in the REVIEW_IN_PROGRESS state."
        delete_stack
        ;;
      *)
        # do nothing
        ;;
    esac
  fi
}
