#!/bin/bash -e

if [ -L /usr/bin/check_stack_final_status ]; then
  . /usr/bin/check_stack_final_status
else
  . $CFN_DEPLOY_PATH/check_stack_final_status.sh
fi

## Required variables
# CFN_STACK_NAME
# CFN_CHANGE_SET_NAME
## Options not implemented
# [--stack-name <value>]
# [--client-request-token <value>]
# [--cli-input-json <value>]
# [--generate-cli-skeleton <value>]

if aws cloudformation execute-change-set --stack-name $CFN_STACK_NAME --change-set-name $CFN_CHANGE_SET_NAME 
then
  cfn-tail $CFN_STACK_NAME
  check_stack_final_status
else
  echo "Change set did not execute properly."
  exit 1
fi
