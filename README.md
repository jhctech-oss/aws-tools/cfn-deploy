# CloudFormation Deployment Project

Contains a bash script for performing CloudFormation stack creation and update based on the current state of the stack. Also provides detailed output for debugging CloudFormation resource states

**If this is your first time visiting this project, check jhctechnology/aws-cloudformation-utilities/awslint> first for an introduction and contribution guidelines.**

## Prerequisites
1. awscli
2. [cfn-tail](https://github.com/taimos/cfn-tail)

## Using the project
We recommend including this project as a git submodule. From your project in a git-compatible terminal, run:

`git submodule add ../relative/path/to/this/project.git`

If hosting your code on a separate repository than this one, specify the full URL instead of the relative path:

`git submodule add https://.../this-project.git`

Set the `CFN_DEPLOY_PATH` variable equal to the relative or absolute path to the folder of the submodule for this project.

### GitLab CI
This project's own [.gitlab-ci-yml](.gitlab-ci-yml) is an excellent starting point.

## Scripts
The following scripts are defined in this project. They are classified as:
* **Type:** CI: this script is primarily used to execute CI jobs
* **Type:** Utility: this script is used by one or multiple CI or Utility scripts as a way to reuse and modularize code segments
* **Type:** CI/Utility: this script defines a function which can be used as a utility by other scripts and also has use cases as a standalone CI job

Since Utility scripts define a function, the script must be sourced (`. ./script_name.sh`) so the function name is available to the bash session, and then the function name can be called to execute (`scipt_name`).

### [check_stack_final_status.sh](check_stack_final_status.sh)
Checks if the stack is in a state which designates a final state; exits with an error if the state is not an end state or if the state is unknown.

**Type:** Utility
#### Required Parameters
* CFN_STACK_NAME

### [create_change_set.sh](create_change_set.sh)
Creates and describes a change set. Can be used to update an existing stack or to create a new stack. The create or update decision will be based on the CFN_STACK_NAME.

**Type:** CI
#### Required Parameters
* CFN_BASE_TEMPLATE
* CFN_STACK_NAME

#### Optional Parameters
* CFN_CHANGE_SET_NAME
* CFN_ROLE_ARN
* CFN_CAPABILITIES
* CFN_CHANGE_SET_DESCRIPTION
* CFN_PARAMETERS_*

### [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
Creates, describes, and then deletes a change set. The main use case for this script is to describe changes made to nested stacks since these changes will not show as a part of the main template's change set. If using this script in that case, it is recommended to define a `describe_only_change_set` job for each nested template, each with their own variables defined per-job. Variables at the job level will override variables defined in a global `variables` block within [.gitlab-ci-yml](.gitlab-ci-yml).

**Type:** CI
#### Required Parameters
* CFN_BASE_TEMPLATE
* CFN_STACK_NAME

#### Optional Parameters
* CFN_CHANGE_SET_NAME
* CFN_ROLE_ARN
* CFN_CAPABILITIES
* CFN_CHANGE_SET_DESCRIPTION
* CFN_PARAMETERS_*

### [delete_change_set.sh](delete_change_set.sh)
Deletes a change set; useful for cleaning up after evaluating a change set if the change set will not be executed in this pipeline

**Type:** CI/Utility
#### Required Parameters
* CFN_STACK_NAME

#### Optional Parameters
* CFN_CHANGE_SET_NAME

### [delete_stack.sh](delete_stack.sh)
Deletes a stack; sets termination protection to false before deletion. **Use with caution** as this operation cannot be interrupted or undone.

**Type:** CI/Utility
### Required Parameters
* CFN_STACK_NAME

### [detect_changes.sh](detect_changes.sh)
Detects whether a change set comes back with resource changes and exits with error if it does. This is best used with GitLab CI's `allow_failure: true` property for jobs so that they stand out as a warning but do not interrupt the CI pipeline (because the changes may be expected).

**Type:** Utility
#### Required Parameters
* CFN_CHANGE_SET_NAME
* CFN_STACK_NAME

### [execute_change_set.sh](execute_change_set.sh)
Executes a preiously-created change set, providing console output of the stack events until some end state is reached.

**Type:** CI/Utility
### Required Parameters
* CFN_STACK_NAME

#### Optional Parameters
* CFN_CHANGE_SET_NAME

### [get_stack_status.sh](get_stack_status.sh)
Gets the current status of the stack at this point in time, including if the stack has been deleted.

**Type:** Utility
### Required Parameters
* CFN_STACK_NAME

## Parameters
We recommend defining these either in your .gitlab-ci.yml as `variables` or as project-level [secret variables](https://docs.gitlab.com/ee/ci/variables/#variables). The main requirement is that the below variables are available to the jobs requiring them. The "required by" and "used by" designate the minimum job set where the variables need to be available.

### Required
1. CFN_DEPLOY_PATH
 - **Required by:** all jobs
 - **Description:** the relative or absolute path within your project workspace to the folder containing this submodule. For most cases this value will just be the name of the project e.g. `CFN_DEPLOY_PATH: cfn-deploy`
2. CFN_BASE_TEMPLATE
 - **Required by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
 - **Description:** path to main template in the form of filename or template url beginning with `https://s3`
3. CFN_STACK_NAME
 - **Required by:** all jobs
 - **Description:** the CloudFormation stack name to assign or the name of an existing stack

### Optional
1. CFN_CHANGE_SET_NAME
 - **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh), [delete_change_set.sh](delete_change_set.sh)
 - A generic name will be chosen if this parameter is not set
2. CFN_ROLE_ARN
 - **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
3. CFN_CAPABILITIES
 - **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
4. CFN_CHANGE_SET_DESCRIPTION
 - **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
5. CFN_PARAMETERS_*
 - **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
 - **Description:** Specify CFN_PROCESSED_PARAMS variable or this value. These values will take precedence over CFN_PROCESSED_PARAMS.
 - **Syntax:** `CFN_PARAMETERS_ParameterKey: ParameterValue`
6. CFN_PROCESSED_PARAMS
 - **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
 - **Description:** Specify a series of CFN_PARAMETER_* variables or this value
 - https://docs.aws.amazon.com/AWSCloudFormation/latest/APIReference/API_Parameter.html
 - https://docs.aws.amazon.com/cli/latest/reference/cloudformation/create-change-set.html

## Optional files
These files can be used to supply additional options to the cfn-deploy scripts:
1. cfn_tags.json
  - **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
2. cfn_rollback_configuration.json
  - **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
3. cfn_resource_types.txt
  - **Used by:** [create_change_set.sh](create_change_set.sh), [create_describe_only_change_set.sh](create_describe_only_change_set.sh)
